# Use the map function to double all the elements in the array.

class ArrayElement

	class << self	
		def double(number_array)
			number_array.split(' ').map { |str_value| element_type(str_value,0) * 2 }
		end

		def double_exception(single_array) 
			single_array.split(' ').map { |char| char = element_type(char,nil); char == nil ? next : char * 2 }.compact
		end

		def element_type(element, return_type)
			if element =~ /^-?\d*(\.\d+)?$/
				element =~ /[^0-9]+/ ? element.to_f  : element.to_i
			else
				element = return_type
			end		
		end
	end
end

puts "Enter number to be doubled in space seprated form"
num_array = gets
print ArrayElement.double_exception(num_array),"\n\n"

=begin
Input 1 4 5 2 56
Output  [2, 8, 10, 4, 112]
=end

=begin
puts "Example working of program"
puts alpha_numeric = "1 2 3 4 5 a 6 7 b  8"
print ArrayElement.double_exception(alpha_numeric),"\n"

puts "Enter the alpanumeric values to be counted"
alpha_numeric = gets
print ArrayElement.double_exception(alpha_numeric),"\n"

Static Input
Example working of program
Input 1 2 3 4 5 a 6 7 b  8
Output [2, 4, 6, 8, 10, 12, 14, 16]

Dynamic Input
Enter the alpanumeric values to be counted
Input 1 2 a b 3 4 c d 5 6
Output [2, 4, 6, 8, 10, 12] 		
=end