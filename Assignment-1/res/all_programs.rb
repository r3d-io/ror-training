class All_programs

  def csv_update
    require 'csv'
    array_to_append = []
    CSV.foreach("./res/input.csv",converters: :numeric) do |row|
      csv_row = [row[0],row[1],row[0]*row[1]]
      array_to_append.push(csv_row)
    end
    CSV.open('./res/output.csv', 'w') do |csv_object|
      array_to_append.each do |row_append|
      csv_object << row_append
      end
    end

    input = CSV.read("./res/input.csv")
    output = CSV.read("./res/output.csv")
    puts "Original Csv"
    print input , "\n"
    puts "Csv after performing operation"
    print "#{output} \n"
  end     

  def printNum()
    (1..100).each do |number|
      print number , " "
    end
    puts
  end

  def countFrequency()
    array_to_count = @array_count.split(" ")
    frequency = Hash.new
    array_to_count.each_with_index do |item,index|
      if frequency.key? item
        frequency[item] += 1
      else    
        frequency[item] = 1
      end    
    end
    print "#{frequency} \n"
  end

  def joinArrNoInbuilt()
    array_one = Array.new() 
    ('a'..'f').each do |char|
      array_one.push(char)
    end 
    array_two = Array [1,2,3,4,5,5,6,7,7,8]
    result = []

    (0...array_one.length + array_two.length).each do |index|
      if index < array_one.length 
        result[index] = array_one[index];
      else
        result[index] = array_two[index - array_one.length]
      end    
    end
    print "#{result} \n"
  end    

  def joinArrInbuilt()
    array_one = @array_one.split(" ")
    array_two = @array_two.split(" ")
    
    result = array_one + array_two
    puts "Array with common element"
    print result , "\n"
    result = array_one | array_two
    puts "Array with unique element"
    print result , "\n"
  end

  def mapDouble()
    single_num_array = number_array.split(' ').map { |str_value| str_value.to_i }
    double_num_array = single_num_array.map! { |single_num| single_num * 2 }
    print "#{double_num_array} \n"
  end    

  def mapDoubleException()
    single_element = @single_array
    double_element = single_element.map { |i|  i*2   }
    print double_element.delete_if {|num| num == 0}
    puts
  end    

  def executeAgain()
      puts "Enter y to execute again n to exit"
      response = gets.chomp 
      if response == 'y' or response == 'Y'
          allPrograms(false)
      elsif response == 'n' or response == 'n' 
          return  
      end
  end        

end
     
def allPrograms(showMessage = true)

    all = All_programs.new()
    if showMessage
    questionString =
        "Enter the program number to view the output
        1. Read from a CSV file, multiple two columns, and then write back to the CSV file.
        2. Write a program that prints from 1 to 100.
        3. Using hash table, print the frequency of occurence of each character inside an array.
        4. Join 2 arrays without using inbuilt functions.
        5. Join 2 arrays using inbuilt functions.
        6. Use the map function to double all the elements in the array.
        7. Use the map function to double all the elements in the array.
            However, handle edge cases (like array can have a character) as well.
        8. To execute another program "
    end
    unless showMessage
       questionString = "Enter the next program number" 
    end
    puts questionString
    number = gets.to_i
    case number
        when 1 
            all.csvUpdate
            all.executeAgain 
        when 2 
            all.printNum
            all.executeAgain
        when 3 
            all.countFrequency
            all.executeAgain       
        when 4 
            all.joinArrNoInbuilt
            all.executeAgain
        when 5 
            all.joinArrInbuilt
            all.executeAgain 
        when 6 
            all.mapDouble
            all.executeAgain
        when 7 
            all.mapDoubleException
            all.executeAgain
        when 8 
            allPrograms
            all.executeAgain    
        else  
            return  
    end
end

allPrograms()