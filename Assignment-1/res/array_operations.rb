# Join 2 arrays using inbuilt functions.

class ArrayOperations
  
  def self.merge_inbuilt(array_one, array_two)
    final_array = array_one.split(' ') + array_two.split(' ')
  end
end        

puts array_one = "a b f g e"
puts array_two = "a b c d e"
print ArrayOperations.merge_inbuilt(array_one, array_two),"\n"

puts "Enter the array 1 "
array_one = gets
puts "Enter the array 2 "
array_two = gets
print ArrayOperations.merge_inbuilt(array_one, array_two),"\n"

=begin
Static input
Input1 a b f g e
Input2 a b c d e
Output final array is ["a", "b", "f", "g", "e", "a", "b", "c", "d", "e"] 

Dynamic input
Enter the array 1 
Input1 1 2 3 4 5 
Enter the array 2 
Input2 a b c d e f
Output final array is ["1", "2", "3", "4", "5", "a", "b", "c", "d", "e", "f"]   
=end