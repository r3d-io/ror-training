# Join 2 arrays without using inbuilt functions.

class Unify 

	class << self
		def array_manual(array_one, array_two)
			result = array_one
			array_two.each {|element| result.push(element)}
			result
		end

		def array_inbuilt(array_one, array_two)
			final_array = array_one.split(' ') + array_two.split(' ')
		end
	end
end

array_one = [] 
array_two = [1, 2, 3, 4, 5, 5, 6, 7, 7, 8]
('a'..'f').each { |char| array_one.push(char) }
print Unify.array_manual(array_one, array_two),"\n"

puts "Enter array one"
array_one = gets.split(" ")
puts "Enter array two"
array_two = gets.split(" ")
print Unify.array_manual(array_one, array_two),"\n\n"


=begin

Static Input
Input1 ["a", "b", "c", "d", "e", "f"]
Input2 [1, 2, 3, 4, 5, 5, 6, 7, 7, 8]
Output ["a", "b", "c", "d", "e", "f", 1, 2, 3, 4, 5, 5, 6, 7, 7, 8]   

Enter array one
Input1 1 7 9 3 5 4 
Enter array two
Input2 a e c w d q 
Output ["1", "7", "9", "3", "5", "4", "a", "e", "c", "w", "d", "q"] 
=end


puts array_one = "a b f g e"
puts array_two = "a b c d e"
print Unify.array_inbuilt(array_one, array_two),"\n"

puts "Enter the array 1 "
array_one = gets
puts "Enter the array 2 "
array_two = gets
print Unify.array_inbuilt(array_one, array_two),"\n"

=begin
Static input
Input1 a b f g e
Input2 a b c d e
Output final array is ["a", "b", "f", "g", "e", "a", "b", "c", "d", "e"] 

Dynamic input
Enter the array 1 
Input1 1 2 3 4 5 
Enter the array 2 
Input2 a b c d e f
Output final array is ["1", "2", "3", "4", "5", "a", "b", "c", "d", "e", "f"]   
=end