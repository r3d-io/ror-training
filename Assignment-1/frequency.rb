# Using hash table, print the frequency of occurence of each character inside an array.

class Frequency 

	class << self
		def element_in_array(array_count)
			frequency_hash = Hash.new(0)
			array_count.each { |item| frequency_hash[item] += 1 }
			frequency_hash
		end

		def count_in_string(user_input)
			frequency_hash = Hash.new(0)
			user_input.each_char { |item| frequency_hash[item] += 1 }
			frequency_hash 
		end
	end	
end


puts "Example working of program"
print array_count = ['a', 'a', 'b', 'b', 1, 1, 'c', 'd', 2],"\n"
print Frequency.element_in_array(array_count),"\n"

puts "Enter the space seperated input to get count of "
array_count = gets.chars
print Frequency.element_in_array(array_count),"\n"

puts "Enter the space seperated input to get count of without array"
array_count = gets
print Frequency.count_in_string(array_count),"\n"

=begin
Static Input  
Example working of program
Input : a a b b 1 1 c d 2 
Output: {"a"=>2, "b"=>2, "1"=>2, "c"=>1, "d"=>1, "2"=>1}

Dynamic Input
Enter the space seperated input to get count of
Input : 1 2 3 a v a 5 a 1 b c 3
Output : {"1"=>2, "2"=>1, "3"=>2, "a"=>3, "v"=>1, "5"=>1, "b"=>1, "c"=>1} 
=end